package S4V.planes.flight;

import S4V.planes.baggage.BaggageRepository;
import S4V.planes.cargo.CargoRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
@Getter
@RequiredArgsConstructor
public class FlightServiceImpl implements FlightService<FlightEntity> {

    private final FlightRepository repository;
    private final CargoRepository cargoRepository;
    private final BaggageRepository baggageRepository;

    @Override
    public Weights weights(LocalDate date, Long flightNumber) {
        Weights weights = new Weights();
        weights.setBaggageWeight(baggageRepository.weight(date,flightNumber)!= null ? baggageRepository.weight(date,flightNumber) : 0);
        weights.setCargoWeight(cargoRepository.weight(date,flightNumber)!= null ? cargoRepository.weight(date,flightNumber) : 0);
        weights.setTotalWeight(totalWeight(date,flightNumber));
        return weights;
    }

    @Override
    public Double totalWeight(LocalDate date, Long flightNumber) {
        if (baggageRepository.weight(date, flightNumber)==null && cargoRepository.weight(date, flightNumber)==null ){
            return 0.0;
        } else if(baggageRepository.weight(date, flightNumber)==null){
            return cargoRepository.weight(date, flightNumber);
        } else if(cargoRepository.weight(date, flightNumber)==null){
            return baggageRepository.weight(date, flightNumber);
        }
        else return baggageRepository.weight(date, flightNumber) + cargoRepository.weight(date, flightNumber);
    }

    @Override
    public FlightEntity add(FlightEntity entity) {
        return repository.save(entity);
    }

    @Override
    public Optional<FlightEntity> get(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<FlightEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public void delete(Long id) {
        repository.delete(repository.getById(id));
    }

    @Override
    public Integer numberOfFlightsArriving(LocalDate date, IATA iata) {
        return repository.numberOfFlightsArriving(date, iata);
    }

    @Override
    public Integer numberOfFlightsDeparting(LocalDate date, IATA iata) {
        return repository.numberOfFlightsDeparting(date, iata);
    }

    @Override
    public Integer totalNumberOfBaggageArriving(LocalDate date, String IATA) {
        return repository.totalNumberOfBaggageArriving(date, IATA);
    }

    @Override
    public Integer totalNumberOfBaggageDeparting(LocalDate date, String IATA) {
        return repository.totalNumberOfBaggageDeparting(date, IATA);
    }
}
