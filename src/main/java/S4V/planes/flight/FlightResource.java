package S4V.planes.flight;

import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "api/flight")
public class FlightResource {

    private final FlightServiceImpl service;

    @GetMapping("/weights")
    @ResponseBody
    public Weights weights(@RequestParam @DateTimeFormat( pattern = "yyyy-MM-dd")  LocalDate date, @RequestParam Long flightNumber){
        return service.weights(date, flightNumber);
    }

    @GetMapping("/total_weight")
    @ResponseBody
    public Double totalWeight(@RequestParam @DateTimeFormat( pattern = "yyyy-MM-dd")  LocalDate date, @RequestParam Long flightNumber){
        return service.totalWeight(date, flightNumber);
    }

    @PostMapping()
    public ResponseEntity<FlightEntity> add(@RequestBody FlightEntity flightEntity) {
        return new ResponseEntity<>(service.add(flightEntity), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<FlightEntity> get(@PathVariable Long id){
        return service.get(id).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping()
    public List<FlightEntity> getAll(){
        return service.getAll();
    }

    @PutMapping("/{id}")
    public ResponseEntity<FlightEntity> replaceFlight(@RequestBody FlightEntity flightEntity, @PathVariable Long id) {

        return service.get(id)
                .map(flight -> {
                    flight.setFlightNumber(flightEntity.getFlightNumber());
                    flight.setDepartureAirportIATACode(flightEntity.getDepartureAirportIATACode());
                    flight.setArrivalAirportIATACode(flightEntity.getArrivalAirportIATACode());
                    flight.setDepartureDate(flightEntity.getDepartureDate());
                    flight.setBaggageEntitySet(flightEntity.getBaggageEntitySet());
                    flight.setCargoEntitySet(flightEntity.getCargoEntitySet());
                    return service.add(flight);
                }).map(ResponseEntity::ok)
                .orElseGet(() ->  ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        service.delete(id);
    }


    @GetMapping("/numberOfFlightsArriving")
    @ResponseBody
    public Integer numberOfFlightsArriving(@RequestParam @DateTimeFormat( pattern = "yyyy-MM-dd") LocalDate date, @RequestParam IATA iata){
        return service.numberOfFlightsArriving(date, iata);
    }

    @GetMapping("/numberOfFlightsDeparting")
    @ResponseBody
    public Integer numberOfFlightsDeparting(@RequestParam @DateTimeFormat( pattern = "yyyy-MM-dd") LocalDate date, @RequestParam IATA iata){
        return service.numberOfFlightsDeparting(date, iata);
    }

    @GetMapping("/totalNumberOfBaggageArriving")
    @ResponseBody
    public Integer totalNumberOfBaggageArriving(@RequestParam @DateTimeFormat( pattern = "yyyy-MM-dd") LocalDate date, @RequestParam String IATA){
        return service.totalNumberOfBaggageArriving(date, IATA);
    }

    @GetMapping("/totalNumberOfBaggageDeparting")
    @ResponseBody
    public Integer totalNumberOfBaggageDeparting(@RequestParam @DateTimeFormat( pattern = "yyyy-MM-dd") LocalDate date, @RequestParam String IATA){
        return service.totalNumberOfBaggageDeparting(date, IATA);
    }
}
