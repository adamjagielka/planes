package S4V.planes.flight;

import S4V.planes.common.Service;

import java.time.LocalDate;

public interface FlightService<T> extends Service<T> {

     Weights weights(LocalDate date, Long flightNumber);
     Double totalWeight(LocalDate date, Long flightNumber);
     Integer numberOfFlightsArriving(LocalDate date, IATA iata);
     Integer numberOfFlightsDeparting(LocalDate date, IATA iata);
     Integer totalNumberOfBaggageArriving(LocalDate date, String IATA);
     Integer totalNumberOfBaggageDeparting(LocalDate date, String IATA);
}
