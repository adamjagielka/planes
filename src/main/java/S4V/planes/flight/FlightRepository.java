package S4V.planes.flight;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface FlightRepository extends JpaRepository<FlightEntity, Long> {



    @Query("select count(f.departureAirportIATACode) from FLIGHT f where f.departureAirportIATACode = :iata and f.departureDate = :departureDate")
    Integer numberOfFlightsDeparting(LocalDate departureDate, IATA iata);

    @Query("select count(f.arrivalAirportIATACode) from FLIGHT f where f.arrivalAirportIATACode = :iata and f.departureDate = :departureDate")
    Integer numberOfFlightsArriving(LocalDate departureDate, IATA iata);

    @Query(nativeQuery = true, value = "select count(b.pieces) from baggage b inner join flight f on b.flight_id = f.id where f.arrival_airport_iata_code = :IATA and f.departure_date = :departureDate")
    Integer totalNumberOfBaggageArriving (LocalDate departureDate, String IATA);

    @Query(nativeQuery = true, value = "select count(b.pieces) from baggage b inner join flight f on b.flight_id = f.id where f.departure_airport_iata_code = :IATA and f.departure_date = :departureDate")
    Integer totalNumberOfBaggageDeparting (LocalDate departureDate, String IATA);
}
