package S4V.planes.flight;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class Weights {
    private Double cargoWeight;
    private Double baggageWeight;
    private Double totalWeight;
}
