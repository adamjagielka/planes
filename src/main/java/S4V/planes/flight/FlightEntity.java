package S4V.planes.flight;

import S4V.planes.baggage.BaggageEntity;
import S4V.planes.cargo.CargoEntity;
import S4V.planes.common.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "FLIGHT")
@Table
@SequenceGenerator(name = "CARGO_SEQ", allocationSize = 1)
public class FlightEntity extends AbstractEntity<Long> {


    @NotNull(message = "flightNumber cannot be null.")
    @Min(value = 1, message = "flightNumber must be a number greater than 0 ")
    @Column(name = "flight_number")
    private Long flightNumber;

//    @Enumerated(EnumType.STRING)
    @NotNull(message = "departure_airport_iata_code cannot be null.")
    @Column(name = "departure_airport_iata_code")
    private IATA departureAirportIATACode;

    @NotNull(message = "arrival_airport_iata_code cannot be null.")
    @Column(name = "arrival_airport_iata_code")
    private IATA arrivalAirportIATACode;

    @NotNull(message = "departure_date cannot be null.")
    @Column(name = "departure_date")
    private LocalDate departureDate;

    @JsonIgnore
    @OneToMany(mappedBy = "FLIGHT")
    private Set<BaggageEntity> baggageEntitySet;

    @JsonIgnore
    @OneToMany(mappedBy = "FLIGHT")
    private Set<CargoEntity> cargoEntitySet;

}
