package S4V.planes.baggage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface BaggageRepository extends JpaRepository<BaggageEntity, Long> {

    @Query(nativeQuery = true, value = "select sel.suma from (SELECT sum (case when b.weight_unit = 0 then (b.weight * b.pieces) when b.weight_unit = 1 then (b.weight * b.pieces /2.205 ) else (b.weight * b.pieces  )  end) as suma FROM baggage b inner join flight f on b.flight_id = f.id where f.flight_number = :flightNumber and f.departure_date = :date group by f.flight_number ) as sel")
    Double weight(LocalDate date, Long flightNumber);
}
