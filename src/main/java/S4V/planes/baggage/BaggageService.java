package S4V.planes.baggage;

import S4V.planes.common.Service;

import java.time.LocalDate;

public interface BaggageService<T> extends Service<T> {

    Double weight(LocalDate date, Long flightNumber);
}
