package S4V.planes.baggage;

import S4V.planes.common.AbstractEntity;
import S4V.planes.common.WeightUnit;
import S4V.planes.flight.FlightEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "BAGGAGE")
@Table
@SequenceGenerator(name = "BAGGAGE_SEQ", allocationSize = 1)
public class BaggageEntity extends AbstractEntity<Long> {

    @NotNull(message = "weight cannot be null")
    @Min(value = 0, message = "the weight cannot be less than 0")
    @Column(name = "weight")
    private Double weight;

    @NotNull(message = "weight unit cannot be null")
    @Column(name = "weight_unit")
    private WeightUnit weightUnit;

    @NotNull(message = "number of pieces cannot be null")
    @Min(value = 1, message = "pieces must be a number greater than 0" )
    @Column(name = "pieces")
    private Integer pieces;

    @NotNull(message = "flight id cannot be null")
    @ManyToOne
    private FlightEntity FLIGHT;
}
