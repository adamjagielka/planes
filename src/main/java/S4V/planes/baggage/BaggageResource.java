package S4V.planes.baggage;

import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "api/baggage")
public class BaggageResource {

    private final BaggageServiceImpl service;



    @PostMapping()
    public ResponseEntity<BaggageEntity> add(@RequestBody BaggageEntity baggageEntity) {
        return new ResponseEntity<>(service.add(baggageEntity), HttpStatus.CREATED) ;
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaggageEntity> get(@PathVariable Long id){
        return service.get(id).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping()
    public List<BaggageEntity> getAll(){
        return service.getAll();
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaggageEntity> replaceBaggage(@RequestBody BaggageEntity baggageEntity, @PathVariable Long id) {

        return service.get(id)
                .map(baggage -> {
                    baggage.setWeightUnit(baggageEntity.getWeightUnit());
                    baggage.setWeight(baggageEntity.getWeight());
                    baggage.setPieces(baggageEntity.getPieces());
                    baggage.setFLIGHT(baggageEntity.getFLIGHT());
                    return service.add(baggage);
                }).map(ResponseEntity::ok)
                .orElseGet(() ->  ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        service.delete(id);
    }

    @GetMapping("/weight")
    @ResponseBody
    public Double baggageWeight(@RequestParam @DateTimeFormat( pattern = "yyyy-MM-dd")  LocalDate date, @RequestParam Long flightNumber){
            return service.weight(date, flightNumber);
        }


}

