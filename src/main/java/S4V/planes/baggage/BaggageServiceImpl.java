package S4V.planes.baggage;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
@Getter
@RequiredArgsConstructor
public class BaggageServiceImpl implements BaggageService<BaggageEntity> {

    private final BaggageRepository repository;

    @Override
    public BaggageEntity add(BaggageEntity entity) {
        return repository.save(entity);
    }

    @Override
    public Optional<BaggageEntity> get(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<BaggageEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public void delete(Long id) {
        repository.delete(repository.getById(id));
    }

    @Override
    public Double weight(LocalDate date, Long flightNumber) {
        return repository.weight(date, flightNumber);
    }


}
