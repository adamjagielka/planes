package S4V.planes.common;

import java.util.List;
import java.util.Optional;

public interface Service<T> {

    T add(T entity);
    Optional<T> get(Long id);
    List<T> getAll();

    void delete(Long id);
}
