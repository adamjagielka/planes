package S4V.planes.cargo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface CargoRepository extends JpaRepository<CargoEntity, Long> {

    @Query(nativeQuery = true, value = "select sel.suma from (SELECT sum (case when c.weight_unit = 0 then (c.weight * c.pieces) when c.weight_unit = 1 then (c.weight * c.pieces /2.205 ) else (c.weight * c.pieces  )  end) as suma FROM cargo c inner join flight f on c.flight_id = f.id where f.flight_number = :flightNumber and f.departure_date = :date group by f.flight_number ) as sel")
    Double weight(LocalDate date, Long flightNumber);
}
