package S4V.planes.cargo;

import S4V.planes.common.Service;

import java.time.LocalDate;

public interface CargoService<T> extends Service<T> {

     Double weight(LocalDate date, Long flightNumber);
}
