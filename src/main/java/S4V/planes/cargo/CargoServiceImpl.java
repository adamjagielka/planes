package S4V.planes.cargo;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
@Getter
@RequiredArgsConstructor
public class CargoServiceImpl implements CargoService<CargoEntity> {

    private final CargoRepository repository;

    @Override
    public Double weight(LocalDate date, Long flightNumber) {
        return repository.weight(date, flightNumber);
    }

    @Override
    public CargoEntity add(CargoEntity entity) {
        return repository.save(entity);
    }

    @Override
    public Optional<CargoEntity> get(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<CargoEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public void delete(Long id) {
        repository.delete(repository.getById(id));
    }
}
