package S4V.planes.cargo;

import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "api/cargo")
public class CargoResource {

    private final CargoServiceImpl service;

    @PostMapping()
    public ResponseEntity <CargoEntity> add(@RequestBody CargoEntity cargoEntity) {
        return new ResponseEntity<>(service.add(cargoEntity), HttpStatus.CREATED) ;
    }

    @GetMapping("/{id}")
    public ResponseEntity<CargoEntity> get(@PathVariable Long id){
        return service.get(id).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping()
    public List<CargoEntity> getAll(){
        return service.getAll();
    }

    @PutMapping("/{id}")
    public ResponseEntity<CargoEntity> replaceCargo(@RequestBody CargoEntity cargoEntity, @PathVariable Long id) {

        return service.get(id)
                .map(cargo -> {
                    cargo.setWeightUnit(cargoEntity.getWeightUnit());
                    cargo.setWeight(cargoEntity.getWeight());
                    cargo.setPieces(cargoEntity.getPieces());
                    cargo.setFLIGHT(cargoEntity.getFLIGHT());
                    return service.add(cargo);
                }).map(ResponseEntity::ok)
                .orElseGet(() ->  ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
         service.delete(id);
    }

    @GetMapping("/weight")
    @ResponseBody
    public Double weight(@RequestParam @DateTimeFormat( pattern = "yyyy-MM-dd") LocalDate date, @RequestParam Long flightNumber){
        return service.weight(date, flightNumber);
    }
}
