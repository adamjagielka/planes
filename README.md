1. **Project name**: "**Planes**"


2. **Introduction**: 
the project includes two main functions:

- For requested Flight Number and date will respond with following :

    - Cargo Weight for requested Flight
    - Baggage Weight for requested Flight
    - Total Weight for requested Flight

- For requested IATA Airport Code and date will respond with following :

    - Number of flights departing from this airport,
    - Number of flights arriving to this airport,
    - Total number (pieces) of baggage arriving to this airport,
    - Total number (pieces) of baggage departing from this airport


3. **Main technologies**:

- Java 11
- Spring
- Postgresql


4. **Run**: 
to start a project:

- create database in Postgresql with:
    - name "planes"
    - username "postgres"
    - password "root"
- run project
